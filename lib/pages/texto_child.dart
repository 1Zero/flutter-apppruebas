import 'package:apppruebas/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Texto',
      home:  MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget{

  MyHomePage();
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Texto de prueba'),
      ),
      body: Center(child: Text(
        'no te rindas gael sigue adelante'
      ),),
    );
  }

}