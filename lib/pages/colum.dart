import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('pruebas de app'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              color: Colors.red,
              height: 100,
              width: 100,
            ),
            Text('estoy en una columna'),
            Container(
              color: Colors.yellow,
              height: 100,
              width: 100,
            ),
            IconButton(
              icon: Icon(
                Icons.video_call,
                color: Colors.red,
              ),
              onPressed: () {
                print('desde una columna');
              },
              iconSize: 100,
            ),
            Icon(
              Icons.android,
              size: 100,
              color: Colors.pink,
            ),
            Image.network(
              'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/3a42d38e-6880-47c2-b4e0-3b22972b6abf/d9bozrd-3f0d7a7b-b8ea-4b36-ba9b-8cf670b7852c.png/v1/fill/w_951,h_840,q_75,strp/lelouch__zero__code_geass_by_calfrills-d9bozrd.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS53YXRlcm1hcmsiXSwib2JqIjpbW3sicGF0aCI6Ii9mLzNhNDJkMzhlLTY4ODAtNDdjMi1iNGUwLTNiMjI5NzJiNmFiZi9kOWJvenJkLTNmMGQ3YTdiLWI4ZWEtNGIzNi1iYTliLThjZjY3MGI3ODUyYy5wbmciLCJ3aWR0aCI6Ijw9OTUxIiwiaGVpZ2h0IjoiPD04NDAifV1dLCJ3bWsiOnsicGF0aCI6Ii93bS8zYTQyZDM4ZS02ODgwLTQ3YzItYjRlMC0zYjIyOTcyYjZhYmYvY2FsZnJpbGxzLTQucG5nIiwib3BhY2l0eSI6OTUsInByb3BvcnRpb25zIjowLjQ1LCJncmF2aXR5IjoiY2VudGVyIn19.mIUToDisspnpq7x_uf5unE3QpK6p0JPusIRUV2tFy2U',
              height: 100,
              width: 100,
            ),
            Switch(value: true, onChanged: (value) {})
          ],
          
        ),
      ),
      drawer: Drawer(),
      endDrawer: Drawer(),
    );
  }
}
