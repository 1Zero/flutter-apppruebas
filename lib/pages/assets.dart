import 'package:apppruebas/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Assets',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar:  AppBar(
        title: Text('Assets'),
      ),
        body: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Image.asset('assets/zero2.jpg',
            height: 100,
            width: 100,
            ),

             Image.asset(
            'assets/zero2.jpg',
            height: 100,
            width: 100,
          ),
           Image.asset(
            'assets/zero2.jpg',
            height: 100,
            width: 100,
          ),

           Image.asset(
            'assets/zero2.jpg',
            height: 100,
            width: 100,
          )
          ],
        ),


    );
  }
}