//import 'package:apppruebas/main.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}
class MyHomePage extends StatelessWidget{
 MyHomePage();
 @override
 Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text('iconos'),
      ),
      body: Center(
        child: IconButton(
          icon: Icon(Icons.close),
          onPressed: (){
            print('sigue adelante');
          },   
        ),
      ),
   );
 }

}