import 'dart:html';

import 'package:apppruebas/main.dart';
import 'package:flutter/material.dart';
 
void main() =>runApp(MyApp());

class MyApp extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),      
    );

  }

}

class MyHomePage extends StatelessWidget{
  MyHomePage();
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar:  AppBar(
        title: Text('imagen'),
        ),
        body: Center(
          child: Image.network(
            'https://www.ecured.cu/images/d/d9/Super.gif',
            height: 900,
            width: 500,
        ),
      ),
    );
  }
}