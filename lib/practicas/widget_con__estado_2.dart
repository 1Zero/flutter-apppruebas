

import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomepage(),
    );
  }
}

class MyHomepage extends StatefulWidget {
  MyHomepage({Key key}) : super(key: key);

  @override
  _MyHomepageState createState() => _MyHomepageState();
}

class _MyHomepageState extends State<MyHomepage> {
  String name = 'gael';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('widget con estado2'),
       ),
       body: Center(
         child: Text(
           this.name,
           style: TextStyle(fontSize: 20),
         ),
       ),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.refresh),
         onPressed:changeName,
       ),
    );
  }

  void changeName(){
    setState(() {
          if (name == 'gael') {
            name =='carlos';
            
          } else {
            name = 'gael';
          }
        });
  }

}

