import 'package:apppruebas/pages/imagenes_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material app',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String name = 'Zero';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('widget con estado1'),         
       ),
       body: Center(
        child: Text(
          this.name,
          style: TextStyle(fontSize: 20),  
        ),   
       ),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.refresh),
         onPressed: changeName,
       ),
    );
  }

void changeName() {
    setState(() {
      if (name == 'Zero') {
        name = 'carlos';
      } else {
        name = 'Zero';
      }
    });
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
    }

    @override
      void dispose() {
        // TODO: implement dispose
        super.dispose();
      }






}


