import 'package:apppruebas/main.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Button column',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('prueba button row'),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.android,
                  color: Colors.pink,
                ),
                onPressed: () {
                  print('desde una prueba');
                },
              ),
              Switch(value: true, onChanged: (value) {}),
              Container(
                color: Colors.blue,
                height: 30,
                width: 30,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Icon(Icons.android),
              ),
              Expanded(child: Text('esto es un expanded')),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.access_alarm,
                  color: Colors.red,
                ),
                onPressed: () {
                  print('desde la 3 fila');
                },
              ),
              Icon(
                Icons.ac_unit,
                color: Colors.amber,
              ),
              Text('spacearound'),
            ],
          ),
          Row(
            children: <Widget>[
              Image.asset(
                'assets/zero2.jpg',
                height: 30,
                width: 30,
              ),
              IconButton(
                icon: Icon(
                  Icons.ac_unit_sharp,
                  color: Colors.brown,
                ),
                onPressed: () {
                  print('desde una 4 fila');
                },
              ),
              Text('cuarta fila'),
              Container(
                height: 30,
                width: 30,
                color: Colors.green,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.accessibility_new_sharp,
                  color: Colors.green,
                ),
                onPressed: () {
                  print('quinta fila');
                },
              ),
              Container(
                height: 30,
                width: 30,
                color: Colors.red,
              ),
              Switch(value: true, onChanged: (value) {})
            ],
          ),
        ],
      ),
    );
  }
}
